var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var path = require('path')

var PORT = 9999;

app.use('/static', express.static(path.join(__dirname, 'static')));

app.use(bodyParser.urlencoded({
  extended: true
}));

app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'public/index.html')));

app.listen(PORT, function (){
    console.log('server running on port ' + PORT + '.')
});