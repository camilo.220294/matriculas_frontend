angular.module("app", []).controller("AppCtrl", function($scope, $http) {
  $scope.students = [];
  $scope.courses = [];
  $scope.enrollments = [];
  $scope.selectedStudent = null;
  $scope.enrollment = null;
  $scope.formActive = false;
  $scope.isEdit = false;
  $scope.error = false;
  $scope.errorMsg = "";

  var PATH = "http://localhost:8080";

  $http({
    method: "GET",
    url: PATH + "/estudiantes/all"
  }).then(
    function(result) {
      $scope.students = result.data;
    },
    function(err) {
      console.log(err);
    }
  );

  $http({
    method: "GET",
    url: PATH + "/cursos/all"
  }).then(
    function(result) {
      $scope.courses = result.data;
    },
    function(err) {
      console.log(err);
    }
  );

  $scope.newEnrollment = function() {
    $scope.formActive = true;
  };

  $scope.save = function() {
    if ($scope.enrollment.id) {
      saveEdit();
      return;
    }
    if (!validateEnrollment()) return;
    
    var data = {
      idcurso: $scope.enrollment.course,
      idestudiante: $scope.enrollment.student,
      calificacion: $scope.enrollment.grade
    };
    $http({
      method: "POST",
      url: PATH + "/matriculas/add",
      params: data,
      transformResponse: [
        function(data) {
          return data;
        }
      ]
    }).then(
      function(result) {
        console.log(result.data);
        $scope.cancel();
        $scope.selectStudent();
      },
      function(err) {
        console.log(err);
      }
    );
  };

  function saveEdit() {
    var data = {
      id: $scope.enrollment.id,      
      calificacion: $scope.enrollment.grade
    };

    if (!validateGradeNull()) return;
    $http({
      method: "POST",
      url: PATH + "/matriculas/update",
      params: data,
      transformResponse: [
        function(data) {
          return data;
        }
      ]
    }).then(
      function(result) {
        console.log(result.data);
        $scope.cancel();
        $scope.selectStudent();
      },
      function(err) {
        console.log(err);
      }
    );
  }

  $scope.edit = function(e) {
    $scope.enrollment = {
      id: e.id,
      course: e.curso.id,
      student: e.estudiante.id,
      grade: e.calificacion
    };
    $scope.formActive = true;
    $scope.isEdit = true;
  };

  $scope.delete = function(id) {
    $http({
      method: "POST",
      url: PATH + "/matriculas/delete",
      params: { id: id },
      transformResponse: [
        function(data) {
          return data;
        }
      ]
    }).then(
      function(result) {
        console.log(result.data);
        $scope.cancel();
        $scope.selectStudent();
      },
      function(err) {
        console.log(err);
      }
    );
  };

  function getCourse(idcourse) {
    var course = $scope.courses.filter(function(item) {
      return item.id == idcourse;
    });

    if (course.length > 0) {
      return course[0];
    }

    return null;
  }

  function getStudent(idstudent) {
    var student = $scope.students.filter(function(item) {
      return item.id == idstudent;
    });

    if (student.length > 0) {
      return student[0];
    }

    return null;
  }

  $scope.selectStudent = function() {
    $http({
      method: "GET",
      url: PATH + "/matriculas/get",
      params: { idestudiante: $scope.selectedStudent }
    }).then(
      function(result) {
        $scope.enrollments = result.data;
      },
      function(err) {
        console.log(err);
      }
    );
  };

  $scope.cancel = function() {
    $scope.formActive = false;
    $scope.enrollment = null;
    $scope.isEdit = false;
    $scope.error = false;
    $scope.errorMsg = "";
  };

  function validateEnrollment() {
    if ($scope.enrollment.course && $scope.enrollment.student && $scope.enrollment.grade) {
      
      return validateGradeNumber();
    }
    $scope.error = true;
    $scope.errorMsg = "Todos los campos son obligatorios";
    return false;
  }

  function validateGradeNumber() {
    if (!isNaN($scope.enrollment.grade)) {
      if ($scope.enrollment.grade >= 0 && $scope.enrollment.grade <= 5) {
        $scope.error = false;
        $scope.errorMsg = "";
        return true;
      } else {
        $scope.error = true;
        $scope.errorMsg = "La calificacion debe set entre 0 y 5";        
      }
    }else {
      $scope.error = true;
      $scope.errorMsg = "La calificacion debe ser un numero";              
    }
    return false;
  }

  function validateGradeNull() {
    if ($scope.enrollment.grade) {
      return validateGradeNumber();
    }
    $scope.error = true;
    $scope.errorMsg = "La calificacion no puede estar vacia.";
    return false;
  }

  
});
